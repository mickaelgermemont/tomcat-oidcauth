package org.bsworks.catalina.authenticator.oidc.tomcat90;

import java.io.IOException;
import java.security.Principal;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;
import org.apache.catalina.authenticator.Constants;
import org.apache.catalina.authenticator.FormAuthenticator;
import org.apache.catalina.connector.Request;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.saml.SAMLCredential;

/**
 * com.on.ps.security.saml.tomcat90.SamlAuthenticator
 * 
 * @author mickaelgermemont
 *
 */
public class SamlAuthenticator extends FormAuthenticator {

	/**
	 * The log.
	 */
	protected final Log log = LogFactory.getLog(this.getClass());
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.catalina.authenticator.FormAuthenticator#doAuthenticate(org.apache
	 * .catalina.connector.Request, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected boolean doAuthenticate(final Request request, final HttpServletResponse response) throws IOException {

		// Authenticate the user making this request, based on the specified login configuration. 
		// Return true if any specified constraint has been satisfied, or false if we have created a response challenge already.

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		final StringBuilder sb = new StringBuilder();
		sb.append("remoteuser=");
		sb.append(request.getRemoteUser());
		
		log.debug(sb.toString());
		
//		response.getOutputStream().print("remoteuser=");
//		response.getOutputStream().print(request.getRemoteUser());
//		response.getOutputStream().println();
		
		final Session session = request.getSessionInternal(true);
		response.getOutputStream().print("sessionid=");
		response.getOutputStream().println(session.getId());
		final HttpSession httpSession = session.getSession();
		
		final Enumeration<String> e = httpSession.getAttributeNames();
		while(e.hasMoreElements()) {
			final String name = e.nextElement();
			response.getOutputStream().print("\t");
			response.getOutputStream().print("session attribute ");
			response.getOutputStream().print(name);
			
			response.getOutputStream().print("\t");
			response.getOutputStream().print("value ");
			final Object value = httpSession.getAttribute(name);
			if(value == null) {
				response.getOutputStream().println("null");
			} else {
				if(name.equals("SPRING_SECURITY_CONTEXT")){
					if(value instanceof org.springframework.security.core.context.SecurityContext) {
						response.getOutputStream().print(" 'instanceOf SecurityContext' ");
						final SecurityContextImpl securityContext = (SecurityContextImpl)value;
						final Authentication auth = securityContext.getAuthentication();
						if(auth!=null) {
							response.getOutputStream().print(" 'Authentication.name' ");
							response.getOutputStream().print(auth.getName());
							response.getOutputStream().print("\t");
							
							final String username = auth.getName();
							
							// get session
							final Principal principal = this.context.getRealm().authenticate(username);
							session.setNote(Constants.FORM_PRINCIPAL_NOTE, principal);

							// successfully reauthenticated, register the principal
							this.log.debug("successfully reauthenticated username \"" + username + "\"");
							this.register(request, response, principal, HttpServletRequest.FORM_AUTH, username, "");
							
							response.sendRedirect(response.encodeRedirectURL("/ebx"));
							
							return true;
							
						} else {
							response.getOutputStream().print(" 'Authentication is null' ");
						}
					} else {
						response.getOutputStream().print(" 'not instanceOf SecurityContext' ");
						response.getOutputStream().print("\t");
						this.getClass().getClassLoader();
						value.getClass().getClassLoader();
						value.getClass().getInterfaces();
					}
					
				}
				
				response.getOutputStream().println(value.toString());
			}
			
		}
		
		if(authentication != null) {
			response.getOutputStream().println("authentication is not null");
			
			final SAMLCredential credential = (SAMLCredential) authentication.getCredentials();
			
			if(credential != null) {
				
				// get session
//				final String username = credential.getNameID().getValue();
//				final Principal principal = this.context.getRealm().authenticate(username);
//				session.setNote(Constants.FORM_PRINCIPAL_NOTE, principal);
//
//				// successfully reauthenticated, register the principal
//				this.log.debug("successfully reauthenticated username \"" + username + "\"");
//				this.register(request, response, principal, HttpServletRequest.FORM_AUTH, username, "");
				
				response.getOutputStream().print("hello1");
				
				//this.forwardToErrorPage(request, response, this.context.getLoginConfig());
				
//				return true;
			}
			
		} else {
			response.getOutputStream().println("authentication is null");
		}
		
		// failure
		// response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		// failure
		
		response.sendError(HttpServletResponse.SC_BAD_REQUEST);
//		this.forwardToErrorPage(request, response, this.context.getLoginConfig());
		
		return false;

	}
}
